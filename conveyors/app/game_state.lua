state={
    room_x=-9999,
    room_y=-9999,
    room='',
    show_counts=false,
    sfx_enabled=true,
    tutorial_enabled=false,
    occupied_belt={}, -- table tracking positions occupied by belts
    actors={}, -- all actors
    production_structures={},
    products={},
    product_pos_map={},
    structures={},
    wave_cells={},
    visible_sprites={},
    current_room_map_sprites={},
    movement_mode='player',
    reference_belt=nil,
    reference_wave=nil,
    reference_frame=0,
    debug=true,
    dialog_mode=false,
    show_coords=true,
    show_keypress=false,
    solid_map_sprites={},
}

player_dummy = nil
