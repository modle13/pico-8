clr_toggled=time()
thing_added=time()
clr_delay=0.5
thing_delay=0.5
clr=12
things={}

function _init()
    poke(24365,1) -- enable mouse
end

function _update()
end

function _draw()
    -- clear screen
    cls()

    -- get mouse x,y
    x=stat(32)
    y=stat(33)
    -- get grid x,y
    grid_x=flr(x/8)*8
    grid_y=flr(y/8)*8

    -- place sprite on LMB
    -- delay prevents adding things too quickly
    -- table prevents adding duplicate things at a grid coordinate
    if stat(34)==1 and time()-thing_added>thing_delay then -- LMB
        things[grid_x..','..grid_y]={sp=1,x=grid_x,y=grid_y}
    end
    -- cycle color on RMB
    if (stat(34)==2 or btnp(4)) and time()-clr_toggled>clr_delay then -- RMB with delay
        set_color(1)
    end
    -- draw grid
    for x=0,15 do
        line(x*8,0,x*8,16*8,clr)
        line(0,x*8,16*8,x*8,clr)
    end
    -- draw things
    for _,t in pairs(things) do
        spr(t.sp,t.x,t.y)
    end
    -- draw cursor
    rectfill(grid_x+3,grid_y+4,grid_x+5,grid_y+4,clr)
    rectfill(grid_x+4,grid_y+3,grid_x+4,grid_y+5,clr)
end

function set_color(n)
    clr+=n
    clr%=15
    if(clr<1)clr=15
    clr_toggled=time()
end

function make_thing(x,y)
end

function draw_thing(n)
    
end
