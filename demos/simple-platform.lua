--globals
player={}
baddie={}
baddies={}
cam={}
globals=
{
    grav=0.2, -- gravity per frame
    debug=true,
    test=false,
}

_min_mem = 9999
_max_mem = 0

-- print debug info to the screen
function draw_debug()
  local _cpu = stat(1)
  local _mem = stat(0)
  local _cpu_c = 11
  local _mem_c = 11

  if (_mem < _min_mem) _min_mem = _mem
  if (_mem > _max_mem) _max_mem = _mem

  if (_cpu > 0.8) _cpu_c = 8
  if (_mem > 250) _mem_c = 8

  print("cpu ".._cpu,0,8,_cpu_c)
  print("mem ".._mem,0,16,_mem_c)
  print("mem min ".._min_mem,0,24,11)
  print("mem max ".._max_mem,0,32,11)
    print("player "..player1:getx(),0,40,11)
    --print("player: "..player1.x..","..player1.y,0,40)
    --print("enemy: "..bad1.x..","..bad1.y,0,48)
end

--create a new camera
function cam:new(mapwidth)
    local o={}
    setmetatable(o, self)
    self.__index=self
    o.x=0
    o.mapwidth=mapwidth
    return o
end

-- move our camera to center on the player
-- todo: follow y location too
function cam:followplayer(playerx)
    self.x=playerx-64.0
    if(self.x<0)self.x=0
    if(self.x>(self.mapwidth-16)*8)self.x=(self.mapwidth-16)*8
    camera(self.x,0)
end

function cam:getx()
    return self.x
end

function cam:reset()
    camera()
end

-- create a baddie
function baddie:new(x,y)
    local o={}
    setmetatable(o, self)
    self.__index = self
    o.x=x
    o.y=y
    o.dx=0
    o.dy=0
    o.spr=032
    o.frms=2
    o.isfaceright=false
    o.bounce=true --do we turn around at a wall?
    o.bad=true
    return o
end

function baddie:draw()
    if self.isfaceright then
        anim(self,self.spr,self.frms,6,true)
    else
        anim(self,self.spr,self.frms,6,false)
    end
end

function baddie:move()
    self.startx=self.x
    if self.isfaceright then
        self.dx=1
    else
        self.dx=-1
    end
    updloc(self)
end

function baddie:update()
end

function player:new(x,y)
    local o={}
    setmetatable(o, self)
    self.__index = self
    o.x=x
    o.y=y
    o.dx=0
    o.dy=0
    o.isgrounded=false
    o.jumpvel=2
    o.isfaceright=true
    o.jumptimer=0 --jump button timer
    o.jumppressed=false --prevent double jumps
    o.score=0
    o.bounce=false --do we turn around at a wall?
    o.bad=false
    o.lives=3
    o.invuln=false
    o.invtimer=0
    o.flash=false
    return o
end

function player:drawlives()
    for i=1, self.lives do
        spr(001,128-(10*i),0)
    end
end

function player:jump()
    self.dy=-self.jumpvel
    self.jumptimer+=1
    sfx(000)
end

function player:extendjump()
    self.dy=-self.jumpvel
    self.jumptimer+=1
end

function player:moveleft()
    self.isfaceright = false
    self.dx=-2
end

function player:moveright()
    self.isfaceright = true
    self.dx=2
end

function player:move()
    --remember where we started
    self.startx=self.x
    self.starty=self.y


    --if player is on the ground start a jump
    if btn(4)
     and self.isgrounded
     and self.jumptimer==0 then
        self:jump()
        self.jumppressed = true
    --if holding down make jump higher
    elseif btn(4)
        and self.jumptimer<10
        and self.jumppressed
        and self.dy < 0 then
        self:extendjump()
    elseif not btn(4) then
        self.jumppressed = false
    end

    self.dx=0
    if btn(0) then --left
        self:moveleft()
    end
    if btn(1) then --right
        self:moveright()
    end
    updloc(self)
end

function player:draw()
    -- only draw the player on a non-flash frame
    if self.flash==false then
        --draw player sprite
        if not self.isgrounded then
            if self.isfaceright then
                spr(6,self.x,self.y,1,1,false)
            else
                spr(6,self.x,self.y,1,1,true)
            end
        elseif self.dx==0 then
            if self.isfaceright then
                spr(1,self.x,self.y,1,1,false)
            else
                spr(1,self.x,self.y,1,1,true)
            end
        elseif self.dx>0 then
            anim(self,1,5,10,false)
        else
            anim(self,1,5,10,true)
        end
    end

    -- flash the player while
    -- invulnerable
    if self.invuln==true then
        if self.flash==true then
            self.flash=false
        else
            self.flash=true
        end
    else
        self.flash=false
    end
end

function player:getx()
    return self.x
end

function player:printscore()
    print("score: "..self.score,0,2,1)
end

function updloc(actor)
    --moveleft/right
    actor.x+=actor.dx
    --accumulate gravity
    actor.dy+=globals.grav
    --fall
    actor.y+=actor.dy
end

function player:collide(actor)
    if actorcollide(self,actor)
        and not self.invuln then
        self.lives-=1
        self.invuln=true
        self.invtimer=100
    end
end

function player:update()
    self.invtimer-=1
    if self.invtimer <= 0 then
        self.invuln = false
    end
end

function intersect(min1, max1, min2, max2)
  return max(min1,max1) > min(min2,max2) and
         min(min1,max1) < max(min2,max2)
end

function actorcollide(actor1,actor2)
    return intersect(actor1.x, actor1.x+8,
        actor2.x, actor2.x+8) and
    intersect(actor1.y, actor1.y+8,
        actor2.y, actor2.y+8)
end

-- object, starting frame, number of frames,
-- animation speed, flip
function anim(actor,sf,nf,sp,fl)
  if(not actor.a_ct) actor.a_ct=0
  if(not actor.a_st) actor.a_st=0
  actor.a_ct+=1
  if(actor.a_ct%(30/sp)==0) then
    actor.a_st+=1
    if(actor.a_st==nf) actor.a_st=0
  end
  actor.a_fr=sf+actor.a_st
  spr(actor.a_fr,actor.x,actor.y,1,1,fl)
end

function checkwallcollision(actor)
    --check for walls in the
    --direction we are moving.
    local xoffset=0
    if actor.dx>0 then xoffset=7 end

    --look for a wall
    local vector1=mget((actor.x+xoffset)/8,(actor.y+7)/8)
    local vector2=mget((actor.x+xoffset)/8,(actor.y)/8)
    if fget(vector1,0) or fget(vector2,0) then
        actor.x=actor.startx
        -- turn bouncers around
        if actor.bounce then
            local h2=mget((actor.x-xoffset)/8,(actor.y+7)/8)
            if not fget(h2,0) then
                if actor.isfaceright then
                    actor.isfaceright = false
                else
                    actor.isfaceright = true
                end
            end
        end
    end


    --check bottom corners of object
    local vertex1=mget((actor.x)/8,(actor.y+8)/8)
    local vertex2=mget((actor.x+7)/8,(actor.y+8)/8)

    --assume they are floating
    --until we determine otherwise
    actor.isgrounded=false

    --only check for floors when
    --moving downward
    if actor.dy>=0 then
        --look for a solid tile
        if fget(vertex1,0) or fget(vertex2,0) then
            --place o on top of tile
            actor.y = flr((actor.y)/8)*8
            --halt velocity
            actor.dy = 0
            --allow jumping again
            actor.isgrounded=true
            actor.jumptimer=0
        end
    end

    --hit ceiling
    --check top corners
    vertex1=mget((actor.x)/8,(actor.y)/8)
    vertex2=mget((actor.x+7)/8,(actor.y)/8)

    --only check for ceilings when
    --moving up
    --if actor.dy<0 then
        --look for solid tile
        if fget(vertex1,0) or fget(vertex2,0) then
            --position p1 right below
            --ceiling
            actor.y = flr((actor.y+8)/8)*8
            --halt upward velocity
            actor.dy = 0
            actor.x=actor.startx
        end
    --end

    -- check if we're touching a pickup
    local h=mget((actor.x+xoffset)/8,(actor.y+7)/8)
    if fget(h,1) and not actor.bad then
        actor.score+=1
        mset((actor.x+xoffset)/8,(actor.y+7)/8,000)
    end
end

function spawn_baddies(plyrx)
 for y=0,16 do
  for x=(plyrx/8)-10,(plyrx/8)+10 do
   val = mget(x,y)
   if fget(val,2) then
    local bad = baddie:new(x*8,y*8)
        add(baddies,bad)
        mset(x,y,0)
   end
  end
 end
end

function count_flags(num)
 local count=0
 for y=0,16 do
  for x=0,64 do
   val = mget(x,y)
   if fget(val,num) then
    count+=1
   end
  end
 end
 return count
end

function _init()
    mapwidth = 64
    mapheight = 16
    mycam = cam:new(mapwidth)
    player1 = player:new(32,72)

end

function _update()
    player1:move()
    player1:update()
    checkwallcollision(player1)
    spawn_baddies(player1:getx())
    for i,actor in pairs(baddies) do
        actor:move()
        checkwallcollision(actor)
        player1:collide(actor)
    end
end

function _draw()
    cls() --clear the screen
    map(0,16,0,0,16,16)
    mycam:followplayer(player1:getx())
    map(16,16,mycam:getx()/1.5,16,128,8)
    map(16,24,mycam:getx()/2,64,38,8)
    map(0,0,0,0,256,128,0x7)
    for i, actor in pairs(baddies) do
        actor:draw()
    end
    player1:draw()
    mycam:reset()
    --player1:printscore()
    --print(globals.test,0,0)
    player1:drawlives()
    if(globals.debug) draw_debug()
end
