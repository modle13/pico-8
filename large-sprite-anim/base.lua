function _init()
    myactor=actor:new(0,2,8,'asdf',0.1,2)
    myactor.sprs={33,49}
    actors={myactor}
end

function _update()
    myactor.dx+=0.01
    foreach(actors,move_actor)
end

function _draw()
    -- clear screen
    cls()

    map()

    foreach(actors,draw_actor)
    handle_camera()
end

function handle_camera()
    camera()
end
